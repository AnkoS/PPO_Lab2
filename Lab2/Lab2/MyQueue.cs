﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    internal class MyQueueElement<T>
    {
        public T Element { set; get; }
        public T Min { set; get; }
        public T Max { set; get; }

        public MyQueueElement() { }
        public MyQueueElement(T element, T min, T max)
        {
            Element = element;
            Min = min;
            Max = max;
        }
    }

    public class MyQueue<T> : IMyQueue<T> where T : IComparable<T>
    {
        private Stack<MyQueueElement<T>> inStack;
        private Stack<MyQueueElement<T>> outStack;
        public int NumberOfElements { private set; get; }
        private int maxSize;

        public MyQueue(int size)
        {
            inStack = new Stack<MyQueueElement<T>>();
            outStack = new Stack<MyQueueElement<T>>();
            maxSize = size;
            NumberOfElements = 0;
        }

        public void Enqueue(T item)
        {
            if (inStack.Count() > 0)
                inStack.Push(new MyQueueElement<T>(item, (item.CompareTo(inStack.Peek().Min) < 0) ? item : inStack.Peek().Min, (item.CompareTo(inStack.Peek().Max)>0)?item:inStack.Peek().Max));
            else
                inStack.Push(new MyQueueElement<T>(item, item, item));


            NumberOfElements++;
            if (NumberOfElements > maxSize)
                Dequeue();
        }

        public T Dequeue()
        {
            if (outStack.Count() == 0)
                while (inStack.Count != 0)
                {
                    MyQueueElement<T> Item = new MyQueueElement<T>();
                    Item.Element = inStack.Pop().Element;
                    if (outStack.Count() > 0)
                    {
                        Item.Max = (Item.Element.CompareTo(outStack.Peek().Max) > 0) ? Item.Element : outStack.Peek().Max;
                        Item.Min = (Item.Element.CompareTo(outStack.Peek().Min) < 0) ? Item.Element : outStack.Peek().Min;
                    }
                    else
                    {
                        Item.Max = Item.Element;
                        Item.Min = Item.Element;
                    }
                    outStack.Push(Item);
                }

            NumberOfElements--;
            return outStack.Pop().Element;
        }

        public T Min()
        {
            if (inStack.Count() == 0)
                return outStack.Peek().Min;
            if (outStack.Count() == 0)
                return inStack.Peek().Min;

            T element1 = inStack.Peek().Min;
            T element2 = outStack.Peek().Min;
            return (element1.CompareTo(element2) < 0) ? element1 : element2;
        }

        public T Max()
        {
            if (inStack.Count() == 0)
                return outStack.Peek().Max;
            if (outStack.Count() == 0)
                return inStack.Peek().Max;

            T element1 = inStack.Peek().Max;
            T element2 = outStack.Peek().Max;
            return (element1.CompareTo(element2) > 0) ? element1 : element2;
        }

        public List<T> GetAllElements()
        {
            List<T> ResultList = new List<T>();
            Stack<MyQueueElement<T>> inStackTmp = new Stack<MyQueueElement<T>>();
            Stack<MyQueueElement<T>> outStackTmp = new Stack<MyQueueElement<T>>();
            MyQueueElement<T> Item = new MyQueueElement<T>();

            while (outStack.Count() != 0)
            {
                Item = outStack.Pop();
                outStackTmp.Push(Item);
                ResultList.Add(Item.Element);
            }

            while (inStack.Count() != 0)
                inStackTmp.Push(inStack.Pop());

            while (inStackTmp.Count() != 0)
            {
                Item = inStackTmp.Pop();
                inStack.Push(Item);
                ResultList.Add(Item.Element);
            }

            while (outStackTmp.Count() != 0)
                outStack.Push(outStackTmp.Pop());

            return ResultList;
        }
    }
}
