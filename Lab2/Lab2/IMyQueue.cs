﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public interface IMyQueue<T> where T : IComparable<T>
    {
        List<T> GetAllElements();
        void Enqueue(T item);
        T Dequeue();
        T Min();
        T Max();
        int NumberOfElements { get; }
    }
}
