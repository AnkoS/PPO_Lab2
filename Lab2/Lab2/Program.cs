﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Lab2
{
    class Program
    {
        static int Min(int a, int b, int c)
        {
            if (a < b)
            {
                if (a < c)
                    return a;
                else
                    return c;
            }
            else
                if (b < c)
                    return b;
                else
                    return c;
        }

        static void Main(string[] args)
        {
            Random rnd = new Random();
            MyQueue<int> Queue = new MyQueue<int>(6);
            List<int> GetQueue = new List<int>();
            List<int> PrintQueue = new List<int>();
            List<int> GenerateQueue = new List<int>();
            int lengthPrintQueue = 0, lengthGenerateQueue = 0, numb1 = 0, numb2 = 0;

            int checkTime, generateTime, printTime;
            checkTime = 100;
            generateTime = rnd.Next(100, 5000);
            printTime = 3000;
            int min;

            while (true)
            {

                while ((generateTime > 0) && (checkTime > 0) && (printTime > 0))
                {
                    min = Min(generateTime, checkTime, printTime);
                    generateTime -= min;
                    checkTime -= min;
                    printTime -= min;
                    Thread.Sleep(min);
                }

                if (generateTime == 0)
                {
                    GenerateQueue.Add(rnd.Next(0, 100));
                    lengthGenerateQueue++;
                    generateTime = rnd.Next(100, 5000);
                }

                if (checkTime == 0)
                {
                    if (lengthGenerateQueue != 0)
                    {
                        PrintQueue.Add(GenerateQueue[numb1++]);
                        lengthPrintQueue++;
                        if (lengthPrintQueue > 1)
                            lengthPrintQueue += 1 - 1;
                        lengthGenerateQueue--;
                    }
                    checkTime = 100;
                }

                if (printTime == 0)
                {
                    if (lengthPrintQueue != 0)
                    {
                        Console.WriteLine();
                        Queue.Enqueue(PrintQueue[numb2++]);
                        lengthPrintQueue--;
                        GetQueue = Queue.GetAllElements();
                        for (int i = 0; i < GetQueue.Count(); i++)
                            Console.Write(GetQueue[i] + " ");
                        Console.WriteLine();
                        Console.WriteLine("Max: " + Queue.Max());
                        Console.WriteLine("Min: " + Queue.Min());
                        Console.WriteLine();
                    }
                    printTime = 3000;
                }
            }
        }
    }
}
