﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Lab2;

namespace Lab2_Tests
{
    [TestClass]
    public class QueueTests
    {
        [TestMethod]
        public void ElementShouldBeAdded()
        {
            IMyQueue<int> queue = new MyQueue<int>(3);

            queue.Enqueue(1);

            Assert.AreEqual(queue.NumberOfElements, 1);
        }

        [TestMethod]
        public void ElementShouldBeRemoved()
        {
            IMyQueue<int> queue = new MyQueue<int>(3);

            queue.Enqueue(1);
            int item = queue.Dequeue();

            Assert.AreEqual(item,  1);
        }

        [TestMethod]
        public void CurrentQueueSizeShouldNotBeBiggerThanMaxSize()
        {
            IMyQueue<int> queue = new MyQueue<int>(1);
            
            queue.Enqueue(1);
            queue.Enqueue(2);

            Assert.AreEqual(queue.NumberOfElements, 1);
        }

        [TestMethod]
        public void ElementShouldBeAddedCorrectlyInTheFullQueue()
        {
            IMyQueue<int> queue = new MyQueue<int>(1);

            queue.Enqueue(1);
            queue.Enqueue(2);

            Assert.AreEqual(queue.Dequeue(), 2);
        }

        [TestMethod]
        public void MaxElementSouldBeCorrect()
        {
            IMyQueue<int> queue = new MyQueue<int>(3);

            queue.Enqueue(1);
            queue.Enqueue(3);
            queue.Enqueue(2);

            Assert.AreEqual(queue.Max(), 3);
        }

        [TestMethod]
        public void MinElementSouldBeCorrect()
        {
            IMyQueue<int> queue = new MyQueue<int>(3);

            queue.Enqueue(3);
            queue.Enqueue(1);
            queue.Enqueue(2);

            Assert.AreEqual(queue.Min(), 1);
        }

        [TestMethod]
        public void ElementsShouldBeReturnedCorrectly()
        {
            IMyQueue<int> queue = new MyQueue<int>(3);

            queue.Enqueue(3);
            queue.Enqueue(1);
            queue.Enqueue(2);

            List<int> trueQueue = new List<int>() { 3, 1, 2 };
            List<int> testQueue = queue.GetAllElements();

            for (int i = 0; i < 3; i++)
                Assert.AreEqual(testQueue[i], trueQueue[i]);
        }
    }
}
